;;; sqrt-block-2.scm
;;;
;;;

(define (sqrt x)
  (define (good-enough? guess)
	(< (abs (- (square guess) x) 0.0001))
	)
  (define (improve guess)
	(average guess (/ x guess))
	)

  (define (sqrt-iter guess)
	(if (good-enough? guess x)
	  guess
	  (sqrt-iter (improve guess x) x))
	)
  (sqrt-iter 1.0)
  )


