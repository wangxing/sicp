练习 1.3 请定义一个过程，它以三个数为参数，返回其中较大的两个数的平方和。

解:
(define (sum-of-square x y) (+ (* x x) (* y y)))

(define (bigger-sum-of-squares x y z)
	(if (> x y) (if (> y z) (sum-of-square x y) (sum-of-square x z))
	(if (> x z) (sum-of-square x y) (sum-of-square y z))
	)
)

