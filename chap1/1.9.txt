练习1.9
下面几个过程各定义了一种加起两个正整数的方法，它们都基于过程inc（它
将参数增加1）和dec（它将参数减少1）。

	(define (+ a b)
		(if (= a 0)
			b
			(inc (+ (dec a) b))))

	
	(define (+ a b)
		(if (= a 0)
			b
			(+ (dec a) (inc b))))

请用代换模型展示这两个过程在求值(+ 4 5)时所产生的计算过程。这些计算过程是
递归的或者迭代的么？

解：
第一种计算方式：
	(+ 4 5)
	(inc (+ 3 5))
	(inc (inc (+ 2 5)))
	(inc (inc (inc (+ 1 5))))
	(inc (inc (inc (inc (+ 0 5)))))
	(inc (inc (inc (inc 5))))
	(inc (inc (inc 6)))
	(inc (inc 7))
	(inc 8)
	9
	
	是个递归计算过程
	

第二种计算方式：
	(+ 4 5)
	(+ 3 6)
	(+ 2 7)
	(+ 1 8)
	(+ 0 9)
	9
	
	是个迭代计算过程

Note：可以使用trace函数跟踪plus调用过程,在调用plus之前，调用 (trace plus) 即可。
