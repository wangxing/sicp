(define (func n)
  (func-iter 2 1 0 n)
  )

(define (func-iter a b c count)
  (if (= count 0)
	c
	(func-iter (+ a (* 2 b) (* 3 c)) a b (- count 1))
	)
  )
