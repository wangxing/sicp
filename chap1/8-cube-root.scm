;;; Chapter 1 Ex1.8 calculate cube root of given number
;;; y = cube-root(x)     ==> y*y*y = x
;;; (/ (+ (/ x (* y y)) (* y 2)) 3)

(define (square x)
  (* x x))

(define (cube x)
  (* (square x) x))

(define (one-third-of x y)
  (/ (+ x y) 3))

(define (improve y x)
  (one-third-of (/ x (square y)) (* 2 y)))

(define (good-enough? y x)
  (< (abs (- 
			(cube y)
			x
			)
		  ) 
	 0.00001
	 )
  )

(define (cube-root-iter y x)
  ( if (good-enough? y x)
	   y
	   (cube-root-iter (improve y x) x)
	   )
  )

(define (cube-root x)
  (cube-root-iter 1.0 x))
